# Stellarium scripts

## Description

This repository contains various scripts that can be loaded into Stellarium. 

## Installation

Any of the scripts in this repository can be copied into your local Stellarium folders. On a linux installation, 
this might be `/usr/share/stellarium/scripts/`. Once the scripts are copied into the proper folder, they will be
automatically loaded by Stellarium on startup.

In Stellarium, you can access these scripts by pressing the associated keyboard shortcut or by going into the 
"Scripts" tab of the "Configurations" window (shortcut: F2). Then find the script in the list and press the "play"
button at the bottom right of the window.

## Files

### Scripts for changing planet

- 0_sun.ssc: Go to the surface of the Sun by pressing SHIFT+0.
- 1_mercury.ssc: Go to the surface of Mercury by pressing SHIFT+1.
- 2_venus.ssc: Go to the surface of Venus by pressing SHIFT+2.
- 3_moon.ssc: Go to the surface of the Moon by pressing SHIFT+3.
- 4_mars.ssc: Go to the surface of Mars by pressing SHIFT+4.
- 5_jupiter.ssc: Go to the surface of Jupiter by pressing SHIFT+5.
- 6_saturn.ssc: Go to the surface of Saturn by pressing SHIFT+6.
- 7_uranus.ssc: Go to the surface of Uranus by pressing SHIFT+7.
- 8_neptune.ssc: Go to the surface of Neptune by pressing SHIFT+8.
- 9_sso.ssc: Go to the Solar System Observer location by pressing SHIFT+9.

### Scripts for changing location coordinates

- go_north.ssc: Change the latitude to go one degree North by pressing Y.
- go_south.ssc: Change the latitude to go one degree South by pressing U.
- go_east.ssc: Change the longitude to go one degree East by pressing CTRL+U.
- go_west.ssc: Change the latitude to go one degree West by pressing CTRL+Y.
- go_up.ssc: Change the altitude to go up by 100 meters by pressing ALT+Y.
- go_down.ssc: Change the altitude to go down by 100 meters by pressing ALT+U.

### Scripts for moving through time
- lunar_month_add.ssc: Increase the time by one lunar month (29.53 days) by pressing ALT+].
- lunar_month_sub.ssc: Decrase the time by one lunar month (29.53 days) by pressing ALT+[.

### Scripts for drawing things on the sky
